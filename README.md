# dockerized IMs #

dockerfiles for skype, telegram, ...

Frequently Asked Questions
==========================

* this section taken from skype's readme of Tom Parys. Applies to telegram too. *

Why would I want to do this?
----------------------------
There are a couple of reasons you might want to restrict Skype's access to your computer:

* It is proprietary Microsoft software
* The skype binary is disguised against decompiling, so nobody is (still) able to reproduce what it really does.
* It produces encrypted traffic even when you are not actively using Skype.
* Skype vulnerability count and status is unclear at best ([indicative incident](http://www.zdnet.com/article/skype-cannot-fix-security-bug-without-a-massive-code-rewrite/)) 
