# -*- mode: dockerfile; coding: utf-8; -*-
# -*- eval: (visual-line-mode t); -*-
# -*- Time-stamp: <2022-08-07 23:23:20 roukoru> -*-
#
# Dockerfile template
#
# exposed ports: 22 -> 8022
# persistent volumes: /home/telegram/.local/share/TelegramDesktop -> `docker volume inspect telegram_desktop`
# login (container ip may change (also, `--ip`)): ssh -Y telegram@172.17.0.2
#
# docker volume create telegram_desktop
# docker build -t telegram:4.0.2 telega
# docker run -it --rm -p 8022:22 --name telega telegram:4.0.2
# docker run -it --rm -p 8022:22 --name telega -v telegram_desktop:/home/telegram/.local/share/TelegramDesktop telegram:4.0.2
# docker container stop telega
#
# TODO:
#   - sound bridging (via networked pulseaudio (see skype))
#

FROM debian
MAINTAINER Korskov Roman

RUN apt-get update

# https://wiki.debian.org/ReduceDebian
RUN apt-get install -y busybox
# Xorg/X11 for Telegram
RUN apt-get install -y libx11-6 libdrm2 libglib2.0-0 libfontconfig libegl1 libgl1 libgtk-3-0
RUN apt-get install -y fonts-wqy-microhei fonts-wqy-zenhei fonts-dejavu fonts-noto
RUN apt-get install -y openssh-server
RUN mkdir /var/run/sshd

#
# login by ssh-keys w/o prompt ...
RUN useradd -m telegram
RUN mkdir /home/telegram/.ssh && chown telegram /home/telegram/.ssh && chmod 700 /home/telegram/.ssh
COPY korskov.pub /home/telegram/.ssh/authorized_keys
RUN chmod 600 /home/telegram/.ssh/authorized_keys && chown telegram /home/telegram/.ssh/authorized_keys

# fetch from https://updates.tdesktop.com/tlinux/tsetup.3.0.1.tar.xz
# (actual version may change)
COPY Telegram /home/telegram/Telegram
RUN chmod 755 /home/telegram/Telegram && chown telegram /home/telegram/Telegram
# ... and set Telegram as user's shell
RUN usermod -s /home/telegram/Telegram telegram
RUN mkdir -p /home/telegram/.local/share/TelegramDesktop && chown -R telegram /home/telegram/.local && chmod go-rwx /home/telegram/.local

RUN printf "umask 077\n" > /etc/profile.d/umask
#RUN printf "LANG='C.UTF-8'\n" > /etc/profile.d/locale
RUN printf "LANG='C.UTF-8'\n" > /etc/environment.d/locale.sh
RUN printf "TZ='Europe/Moscow'\n" > /etc/timezone
RUN rm /etc/localtime; ln -s /usr/share/zoneinfo/Europe/Moscow /etc/localtime

EXPOSE 22
# use those 2 lines ...
CMD ["/usr/sbin/sshd", "-4Dd"]
ENTRYPOINT []
# ... or this to debug container, exclusive
# ENTRYPOINT ["/bin/busybox", "sh"]
