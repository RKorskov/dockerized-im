# -*- mode: dockerfile; coding: utf-8; -*-
# -*- eval: (visual-line-mode t); -*-
# -*- Time-stamp: <2022-08-01 13:46:47 korskov> -*-
#
# Dockerfile template
#
# exposed ports: 22 -> 8122
# persistent volumes: /home/Discord/.config -> `docker volume inspect discord`
# ? only ~/.config/discord
# ? ~/.pki
# login: ssh -Y -p 8122 discord@localhost
#
# docker volume create discord
# docker build -t discord:0.0.18 discord
# docker run -it --rm -p 8122:22 --name discord --network SpiralSkein discord:0.0.18
# docker run -it --rm -p 8122:22 --name discord -v discord:/mnt/Discord.config --network SpiralSkein discord:0.0.18
# docker container stop discord
#
# TODO:
#   - sound bridging (via networked pulseaudio (see skype))
#

FROM debian
MAINTAINER Korskov Roman

RUN apt-get update

# https://wiki.debian.org/ReduceDebian
RUN apt-get install -y busybox
RUN apt-get install -y libx11-6 libdrm2 libglib2.0-0 libfontconfig libegl1 libgl1
RUN apt-get install -y fonts-wqy-microhei fonts-wqy-zenhei fonts-dejavu fonts-noto
RUN apt-get install -y openssh-server
RUN apt-get install -y libgtk-3.0 libnss3 libasound2
RUN mkdir /var/run/sshd

RUN printf "umask 077\n" > /etc/profile.d/umask
RUN printf "LANG=C.UTF-8\n" > /etc/profile.d/locale

RUN useradd -m -d /home/Discord discord
#
# login by ssh-keys w/o prompt ...
RUN mkdir /home/Discord/.ssh && chown discord /home/Discord/.ssh && chmod 0700 /home/Discord/.ssh
# ... your ~/.ssh/id_ecdsa.pub goes into .ssh/authorized_keys
COPY discord-host-user.pub /home/Discord/.ssh/authorized_keys
RUN chmod 600 /home/Discord/.ssh/authorized_keys && chown discord /home/Discord/.ssh/authorized_keys
# persistent .config dir
RUN rm -rf /home/Discord/.config && mkdir -p /mnt/Discord.config && ln -s /mnt/Discord.config/config /home/Discord/.config

# fetch from https://dl.discordapp.net/apps/linux/0.0.18/discord-0.0.18.tar.gz
# (actual version may change)
COPY discord-0.0.18.tar.gz /discord-latest.tar.gz
RUN tar x -f /discord-latest.tar.gz -C /home
RUN chmod 0755 /home/Discord/Discord && chown -R discord /home/Discord
RUN chown root /home/Discord/chrome-sandbox && chmod 04755 /home/Discord/chrome-sandbox
RUN usermod -s /bin/sh discord && printf "exec /home/Discord/Discord --no-sandbox\n" > /home/Discord/.profile

EXPOSE 22
# use those 2 lines ...
CMD ["/usr/sbin/sshd", "-4Dd"]
ENTRYPOINT []
# ... or this to debug container, exclusive
#ENTRYPOINT ["/bin/busybox", "sh"]
# ... or docker exec -it discord /bin/bash
