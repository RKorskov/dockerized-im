#! /bin/sh

TELE_PORT=8022

case $1 in
    run)
        exec docker run -it --rm -p ${TELE_PORT}:22 --name telega -v telegram_desktop:/home/telegram/.local/share/TelegramDesktop --network SpiralSkein telegram:4.0.2
        ;;
    build)
        exec docker build -t telegram:4.0.2 .
        ;;
    prebuild)
        docker volume create telegram_desktop
        docker network create SpiralSkein
        ;;
    login)
        #addr=`docker network inspect SpiralSkein | jq '.[]."Containers"|.[]."Name"="telega"|.[]."IPv4Address"'  | tr -d '"' | cut -d '/' -f 1`
        #exec ssh -Y -p 8022 "telegram@${addr}"
        exec ssh -Y -p ${TELE_PORT} telegram@localhost
        ;;
    *)
        printf "Actions:\n"
        printf "\t run\n"
        printf "\t build\n"
        printf "\t login\n"
esac
